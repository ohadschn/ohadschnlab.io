The other day I encoutnered the following sequence of events:

1. A colleague introduced RBAC-based authorizatio to our service
1. Our test in production (TIP) system started encountering 403 responses
1. When granted the proper roles, devs running the TIP tests using their credentials were able to authorize successfully
1. The actual cloud-run TIP excutions using service principal (SP) credentials were continuing to fail even after SPs have been granted the proper roles
1. Upon inspecting the SP roles, there was strange behavior were the clicking the SP in the portal would redirect to "404"

The situation seemed clear - there was something wrong with the SP roles, right?
It can't be a coincidence that on the exact day we enabled RBAC we started getting 403, dev vreds work, sp creds fail and something is wrong with their association in the portal right? well, turns out it can be a coincidence. Opening a ticket for Azure Active Direcoty was a waste of time.